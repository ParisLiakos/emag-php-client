<?php

namespace EmagPhpClient;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class Emag
{
    protected $marketplaceClient;

    public function getMarketplaceClient(string $url, string $username, string $password): Marketplace\Client
    {
        if (!$this->marketplaceClient) {
            $this->marketplaceClient = new Marketplace\Client($url, $username, $password, new Client);
        }

        return $this->marketplaceClient;
    }
}
