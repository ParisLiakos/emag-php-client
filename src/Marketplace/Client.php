<?php

namespace EmagPhpClient\Marketplace;

use GuzzleHttp\ClientInterface;

class Client
{
  protected $url = 'https://marketplace.emag.bg/api-3';
  protected $username;
  protected $password;
  protected $httpClient;

  public function __construct(string $url, string $username, string $password, ClientInterface $httpClient) {
    $this->url = $url;
    $this->username = $username;
    $this->password = $password;
    $this->httpClient = $httpClient;
  }

  protected function get(string $uri): Response
  {
    $data = [
        'auth' => [
            $this->username,
            $this->password,
        ],
    ];
    $response = $this->httpClient->get($this->url . $uri, $data);

    return new Response($response->getBody());
  }

  protected function post(string $uri, array $data): Response
  {
    $data = [
        'auth' => [
            $this->username,
            $this->password,
        ],
        'json' => [
          'Key' => __METHOD__,
          'data' => $data,
        ],
    ];
    $response = $this->httpClient->post($this->url . $uri, $data);

    return new Response($response->getBody());
  }

  public function getCategories(): array {
    $response = $this->get('/category/read');

    if ($response->isError()) {
      throw new \RuntimeException(sprintf('emag API error: %s', var_export($response->getMessages(), true)));
    }

    return $response->getResults();
  }

  public function getProducts(int $currentPage = 1, int $itemsPerPage = 100): array {
    $response = $this->post('/product_offer/read', [
      'currentPage' => $currentPage,
      'itemsPerPage' => $itemsPerPage,
    ]);

    if ($response->isError()) {
      throw new \RuntimeException(sprintf('emag API error: %s', var_export($response->getMessages(), true)));
    }

    return $response->getResults();
  }
}
