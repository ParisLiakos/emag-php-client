<?php

namespace EmagPhpClient\Marketplace;

class Response {
  private $raw;
  protected $isError;

  public function __construct(string $raw) {
    $this->raw = $raw;
    $decoded = json_decode($raw);
    if (json_last_error()) {
      throw new \RuntimeException(sprintf('Emag response did not contain valid JSON: %s', json_last_error_msg()));
    }

    $this->isError = (bool) $decoded->isError;
    $this->messages = $decoded->messages;
    $this->results = $decoded->results;
  }

  public function isError(): bool {
    return $this->isError;
  }

  public function getMessages(): array {
    return $this->messages;
  }

  public function getResults(): array {
    return $this->results;
  }
}
